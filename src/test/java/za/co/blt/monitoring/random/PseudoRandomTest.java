package za.co.blt.monitoring.random;

import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class PseudoRandomTest {

    @Test
    public void testPseudoRandomNumber() throws Exception {
        PseudoRandom pr = new PseudoRandom();

        Field pseudoRandomNumbersField = PseudoRandom.class.getDeclaredField("pseudoRandomNumbers");
        pseudoRandomNumbersField.setAccessible(true);
        ArrayList<Integer> pseudoRandomNumbers = (ArrayList<Integer>) pseudoRandomNumbersField.get(pr);

        for (int i = 0; i < pseudoRandomNumbers.size(); i++) {
            assertEquals(pseudoRandomNumbers.get(i), pr.nextInt());
        }

        assertEquals(pseudoRandomNumbers.get(0), pr.nextInt());
    }
}
