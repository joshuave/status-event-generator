package za.co.blt.monitoring.genarator.factory;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class TransactionOutcomeFactoryTest {

    @Test
    public void transactionOutcomeFactory() {
        assertEquals(TransactionOutcomeFactory.TRANSACTION_OUTCOME_SUCCESSFUL, TransactionOutcomeFactory.getOutcome(0));
        assertEquals(TransactionOutcomeFactory.TRANSACTION_OUTCOME_SUCCESSFUL, TransactionOutcomeFactory.getOutcome(3));
        assertEquals(TransactionOutcomeFactory.TRANSACTION_OUTCOME_FAILED_TECHNICAL_ERROR, TransactionOutcomeFactory.getOutcome(1));
        assertEquals(TransactionOutcomeFactory.TRANSACTION_OUTCOME_FAILED_TECHNICAL_ERROR, TransactionOutcomeFactory.getOutcome(4));
        assertEquals(TransactionOutcomeFactory.TRANSACTION_OUTCOME_FAILED_BUSINESS_RULE_ERROR, TransactionOutcomeFactory.getOutcome(2));
        assertEquals(TransactionOutcomeFactory.TRANSACTION_OUTCOME_FAILED_BUSINESS_RULE_ERROR, TransactionOutcomeFactory.getOutcome(5));
    }
}