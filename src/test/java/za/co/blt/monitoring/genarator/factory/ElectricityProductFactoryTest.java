package za.co.blt.monitoring.genarator.factory;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class ElectricityProductFactoryTest {

    @Test
    public void electricityProductFactory() {
        ElectricityProductFactory epf = new ElectricityProductFactory();

        assertEquals(ElectricityProductFactory.ELECTRICITY_PRODUCT_AFHCO, epf.getProduct(0));
        assertEquals(ElectricityProductFactory.ELECTRICITY_PRODUCT_AFHCO, epf.getProduct(19));
        assertEquals(ElectricityProductFactory.ELECTRICITY_PRODUCT_BELA_BELA, epf.getProduct(1));
        assertEquals(ElectricityProductFactory.ELECTRICITY_PRODUCT_BELA_BELA, epf.getProduct(20));
        assertEquals(ElectricityProductFactory.ELECTRICITY_PRODUCT_BFW_METERING, epf.getProduct(2));
        assertEquals(ElectricityProductFactory.ELECTRICITY_PRODUCT_BFW_METERING, epf.getProduct(21));
        assertEquals(ElectricityProductFactory.ELECTRICITY_PRODUCT_BLUEBERRY, epf.getProduct(3));
        assertEquals(ElectricityProductFactory.ELECTRICITY_PRODUCT_BLUEBERRY, epf.getProduct(22));
        assertEquals(ElectricityProductFactory.ELECTRICITY_PRODUCT_BROHAM, epf.getProduct(4));
        assertEquals(ElectricityProductFactory.ELECTRICITY_PRODUCT_BROHAM, epf.getProduct(23));
        assertEquals(ElectricityProductFactory.ELECTRICITY_PRODUCT_CITYPOWER, epf.getProduct(5));
        assertEquals(ElectricityProductFactory.ELECTRICITY_PRODUCT_CITYPOWER, epf.getProduct(24));
        assertEquals(ElectricityProductFactory.ELECTRICITY_PRODUCT_DIKGATLONG, epf.getProduct(6));
        assertEquals(ElectricityProductFactory.ELECTRICITY_PRODUCT_DIKGATLONG, epf.getProduct(25));
        assertEquals(ElectricityProductFactory.ELECTRICITY_PRODUCT_EKURHULENI, epf.getProduct(7));
        assertEquals(ElectricityProductFactory.ELECTRICITY_PRODUCT_EKURHULENI, epf.getProduct(26));
        assertEquals(ElectricityProductFactory.ELECTRICITY_PRODUCT_EMALAHLENI, epf.getProduct(8));
        assertEquals(ElectricityProductFactory.ELECTRICITY_PRODUCT_EMALAHLENI, epf.getProduct(27));
        assertEquals(ElectricityProductFactory.ELECTRICITY_PRODUCT_ETHEKWINI, epf.getProduct(9));
        assertEquals(ElectricityProductFactory.ELECTRICITY_PRODUCT_ETHEKWINI, epf.getProduct(28));
        assertEquals(ElectricityProductFactory.ELECTRICITY_PRODUCT_GOVAN_MBEKI, epf.getProduct(10));
        assertEquals(ElectricityProductFactory.ELECTRICITY_PRODUCT_GOVAN_MBEKI, epf.getProduct(29));
        assertEquals(ElectricityProductFactory.ELECTRICITY_PRODUCT_HBROS, epf.getProduct(11));
        assertEquals(ElectricityProductFactory.ELECTRICITY_PRODUCT_HBROS, epf.getProduct(30));
        assertEquals(ElectricityProductFactory.ELECTRICITY_PRODUCT_KOKSTAD, epf.getProduct(12));
        assertEquals(ElectricityProductFactory.ELECTRICITY_PRODUCT_KOKSTAD, epf.getProduct(31));
        assertEquals(ElectricityProductFactory.ELECTRICITY_PRODUCT_LANDISANDGYR, epf.getProduct(13));
        assertEquals(ElectricityProductFactory.ELECTRICITY_PRODUCT_LANDISANDGYR, epf.getProduct(32));
        assertEquals(ElectricityProductFactory.ELECTRICITY_PRODUCT_LANDIS_PREPAID, epf.getProduct(14));
        assertEquals(ElectricityProductFactory.ELECTRICITY_PRODUCT_LANDIS_PREPAID, epf.getProduct(33));
        assertEquals(ElectricityProductFactory.ELECTRICITY_PRODUCT_LL_ENERGY, epf.getProduct(15));
        assertEquals(ElectricityProductFactory.ELECTRICITY_PRODUCT_LL_ENERGY, epf.getProduct(34));
        assertEquals(ElectricityProductFactory.ELECTRICITY_PRODUCT_MBOMBELA, epf.getProduct(16));
        assertEquals(ElectricityProductFactory.ELECTRICITY_PRODUCT_MBOMBELA, epf.getProduct(35));
        assertEquals(ElectricityProductFactory.ELECTRICITY_PRODUCT_METER_SHACK, epf.getProduct(17));
        assertEquals(ElectricityProductFactory.ELECTRICITY_PRODUCT_METER_SHACK, epf.getProduct(36));
        assertEquals(ElectricityProductFactory.ELECTRICITY_PRODUCT_TSHWANE, epf.getProduct(18));
        assertEquals(ElectricityProductFactory.ELECTRICITY_PRODUCT_TSHWANE, epf.getProduct(37));
    }
}