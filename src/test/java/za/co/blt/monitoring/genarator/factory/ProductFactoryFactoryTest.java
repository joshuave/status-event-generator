package za.co.blt.monitoring.genarator.factory;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class ProductFactoryFactoryTest {

    @Test
    public void productCategory() {
        assertEquals(AirtimeProductFactory.class, ProductFactoryFactory.productFactory(0).getClass());
        assertEquals(AirtimeProductFactory.class, ProductFactoryFactory.productFactory(2).getClass());
        assertEquals(ElectricityProductFactory.class, ProductFactoryFactory.productFactory(1).getClass());
        assertEquals(ElectricityProductFactory.class, ProductFactoryFactory.productFactory(3).getClass());
    }
}