package za.co.blt.monitoring.genarator.factory;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class AirtimeProductFactoryTest {

    @Test
    public void airtimeProductFactory(){
        AirtimeProductFactory apf = new AirtimeProductFactory();
        assertEquals(AirtimeProductFactory.AIRTIME_PRODUCT_CELLC, apf.getProduct(0));
        assertEquals(AirtimeProductFactory.AIRTIME_PRODUCT_CELLC, apf.getProduct(5));
        assertEquals(AirtimeProductFactory.AIRTIME_PRODUCT_MTN, apf.getProduct(1));
        assertEquals(AirtimeProductFactory.AIRTIME_PRODUCT_MTN, apf.getProduct(6));
        assertEquals(AirtimeProductFactory.AIRTIME_PRODUCT_TELKOM, apf.getProduct(2));
        assertEquals(AirtimeProductFactory.AIRTIME_PRODUCT_TELKOM, apf.getProduct(7));
        assertEquals(AirtimeProductFactory.AIRTIME_PRODUCT_VIRGIN, apf.getProduct(3));
        assertEquals(AirtimeProductFactory.AIRTIME_PRODUCT_VIRGIN, apf.getProduct(8));
        assertEquals(AirtimeProductFactory.AIRTIME_PRODUCT_VODACOM, apf.getProduct(4));
        assertEquals(AirtimeProductFactory.AIRTIME_PRODUCT_VODACOM, apf.getProduct(9));
    }
}