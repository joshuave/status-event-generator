package za.co.blt.monitoring.genarator.factory;

import org.junit.jupiter.api.Test;
import za.co.blt.monitoring.genarator.model.TransactionStatusEvent;

import java.io.IOException;
import java.net.URISyntaxException;

import static org.junit.jupiter.api.Assertions.*;


class TransactionStatusEventFactoryTest {

    @Test
    public void transactionStatusEventFactory() throws IOException, URISyntaxException {
        TransactionStatusEventFactory transactionStatusEventFactory = new TransactionStatusEventFactory();

        for (int i = 0; i < 100; i++) {
            TransactionStatusEvent transactionStatusEvent = transactionStatusEventFactory.generate();
            assertNotNull(transactionStatusEvent);
            assertNotNull(transactionStatusEvent.getProductCategory());
            assertNotNull(transactionStatusEvent.getProduct());
            assertNotNull(transactionStatusEvent.getOutcome());
        }
    }
}