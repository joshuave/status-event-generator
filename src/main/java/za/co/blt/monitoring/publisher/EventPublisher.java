package za.co.blt.monitoring.publisher;

import za.co.blt.monitoring.genarator.model.TransactionStatusEvent;


public interface EventPublisher {

    public void publishEvent(TransactionStatusEvent transactionStatusEvent) throws Exception;
}
