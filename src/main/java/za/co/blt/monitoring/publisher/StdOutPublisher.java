package za.co.blt.monitoring.publisher;

import com.fasterxml.jackson.databind.ObjectMapper;
import za.co.blt.monitoring.genarator.model.TransactionStatusEvent;



public class StdOutPublisher implements EventPublisher {

    private static final ObjectMapper om = new ObjectMapper();

    public StdOutPublisher() {
        System.out.println("Running with StdOutPublisher");
    }

    @Override
    public void publishEvent(TransactionStatusEvent transactionStatusEvent) throws Exception {
        System.out.println(om.writeValueAsString(transactionStatusEvent));
    }
}
