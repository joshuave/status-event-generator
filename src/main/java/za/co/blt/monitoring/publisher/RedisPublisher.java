package za.co.blt.monitoring.publisher;

import redis.clients.jedis.Jedis;
import za.co.blt.monitoring.genarator.model.TransactionStatusEvent;

import java.util.HashMap;
import java.util.Map;


public class RedisPublisher implements EventPublisher {

    private Jedis jedis;
    private String streamKey;

    public RedisPublisher(String hostname, int port, String streamKey) {
        this.jedis = new Jedis(hostname, port);
        this.streamKey = streamKey;
        System.out.println("Running with RedisPublisher. Publishing to \"" + streamKey + "\" stream.");
    }

    @Override
    public void publishEvent(TransactionStatusEvent transactionStatusEvent) throws Exception {
        this.jedis.xadd(this.streamKey, null, this.translateFrom(transactionStatusEvent));
        System.out.println("Published event to redis: " + transactionStatusEvent);
    }


    private Map<String, String> translateFrom(TransactionStatusEvent transactionStatusEvent) {
        Map<String, String> dataMap = new HashMap<>();
        dataMap.put("product_category", transactionStatusEvent.getProductCategory());
        dataMap.put("product", transactionStatusEvent.getProduct());
        dataMap.put("outcome", transactionStatusEvent.getOutcome());
        return  dataMap;
    }

}
