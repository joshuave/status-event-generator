package za.co.blt.monitoring.random;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class PseudoRandom {

    private static final String FILE_PATH = "pseudo_random_numbers.txt";
    private List<Integer> pseudoRandomNumbers;
    private int listOffset;

    public PseudoRandom() throws IOException, URISyntaxException {
        this.pseudoRandomNumbers = new ArrayList<>();
        this.listOffset = 0;

        final URI uri = PseudoRandom.class.getClassLoader().getResource(FILE_PATH).toURI();
        FileSystem fs = null;
        Path path = null;
        try {
            if ("jar".equals(uri.getScheme())) {
                final Map<String, String> env = new HashMap<>();
                final String[] array = uri.toString().split("!");
                fs = FileSystems.newFileSystem(URI.create(array[0]), env);
                path = fs.getPath(array[1]);
            } else {
                path = Paths.get(uri);
            }

            Files.readAllLines(path).forEach(s -> {
                try {
                    Integer i = Integer.parseInt(s);
                    this.pseudoRandomNumbers.add(i);
                } catch (NumberFormatException ex) {
                    // log warning and swallow
                }
            });
        } finally {
            if (null != fs) {
                fs.close();
            }
        }
    }

    public int nextInt() {
        if (this.pseudoRandomNumbers.size() <= this.listOffset) {
            this.listOffset = 0;
        }
        int i = this.pseudoRandomNumbers.get(this.listOffset);
        this.listOffset++;
        return i;
    }
}
