package za.co.blt.monitoring;

import za.co.blt.monitoring.genarator.factory.EventPublisherFactory;
import za.co.blt.monitoring.genarator.factory.TransactionStatusEventFactory;
import za.co.blt.monitoring.publisher.EventPublisher;
import za.co.blt.monitoring.random.PseudoRandom;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.InputMismatchException;


public class Main {

    private static final String USAGE_MESSAGE = "Expected parameters are either\n" +
            "-s 'use StdOutPublisher that publishes events to stdout\n" +
            "or\n" +
            "-r <host> <post> <stream key> 'use RedisPublisher that publishes events to a stream with name <stream key> running on <host>:<port>";

    public static void main( String[] args ) throws IOException, URISyntaxException {

        PseudoRandom pr = new PseudoRandom();
        TransactionStatusEventFactory transactionStatusEventFactory = new TransactionStatusEventFactory();

        try {
            validateCommandlineArguments(args);

            EventPublisher eventPublisher = EventPublisherFactory.eventPublisher(args);

            while (true) {
                try {
                    eventPublisher.publishEvent(transactionStatusEventFactory.generate());
                    Thread.sleep(pr.nextInt() * 10);
                } catch (Exception ex) {
                    ex.printStackTrace();
                    // swallow error
                }
            }
        } catch (InputMismatchException ex) {
            System.out.println(ex.getMessage());
        }
    }

    private static void validateCommandlineArguments(String[] args) {
        if (args.length == 0) {
            throw new InputMismatchException(USAGE_MESSAGE);
        }
        if (args[0].equals("-s")) {
            return;
        } else if (args[0].equals("-r")) {
            if (args.length != 4) {
                throw new InputMismatchException("Expected parameters are <host>, <port> and <topic name>");
            }
            try {
                Integer.parseInt(args[2]);
            } catch (NumberFormatException ex) {
                throw new InputMismatchException("<port> must be an integer");
            }
        }
    }

}
