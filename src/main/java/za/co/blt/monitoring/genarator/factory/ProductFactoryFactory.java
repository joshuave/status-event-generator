package za.co.blt.monitoring.genarator.factory;


public class ProductFactoryFactory {

    private static final ProductFactory PRODUCT_FACTORY_AIRTIME = new AirtimeProductFactory();
    private static final ProductFactory PRODUCT_FACTORY_ELECTRICITY = new ElectricityProductFactory();

    public static ProductFactory productFactory(int randomNumber) {
        ProductFactory productFactory = null;

        int categoryCase = randomNumber % 2;

        switch (categoryCase) {
            case 0: {
                productFactory = PRODUCT_FACTORY_AIRTIME;
                break;
            }
            case 1: {
                productFactory = PRODUCT_FACTORY_ELECTRICITY;
                break;
            }
        }

        return productFactory;
    }
}
