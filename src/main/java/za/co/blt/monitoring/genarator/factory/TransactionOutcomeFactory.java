package za.co.blt.monitoring.genarator.factory;


public class TransactionOutcomeFactory {

    public static final String TRANSACTION_OUTCOME_SUCCESSFUL = "SUCCESSFUL";
    public static final String TRANSACTION_OUTCOME_FAILED_TECHNICAL_ERROR = "FAILED_TECHNICAL_ERROR";
    public static final String TRANSACTION_OUTCOME_FAILED_BUSINESS_RULE_ERROR = "FAILED_BUSINESS_RULE_ERROR";

    public static String getOutcome(int randomNumber) {
        String transactionCategory = null;

        int categoryCase = randomNumber % 3;

        switch (categoryCase) {
            case 0: {
                transactionCategory = TRANSACTION_OUTCOME_SUCCESSFUL;
                break;
            }
            case 1: {
                transactionCategory = TRANSACTION_OUTCOME_FAILED_TECHNICAL_ERROR;
                break;
            }
            case 2: {
                transactionCategory = TRANSACTION_OUTCOME_FAILED_BUSINESS_RULE_ERROR;
                break;
            }
        }

        return transactionCategory;
    }
}
