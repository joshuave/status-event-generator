package za.co.blt.monitoring.genarator.factory;


public class ElectricityProductFactory implements ProductFactory {

    public static final String PRODUCT_CATEGORY = "electricity";

    public static final String ELECTRICITY_PRODUCT_AFHCO = "AFHCO";
    public static final String ELECTRICITY_PRODUCT_BELA_BELA = "Bela Bela";
    public static final String ELECTRICITY_PRODUCT_BFW_METERING = "BFW METERING";
    public static final String ELECTRICITY_PRODUCT_BLUEBERRY = "Blueberry";
    public static final String ELECTRICITY_PRODUCT_BROHAM = "Broham";
    public static final String ELECTRICITY_PRODUCT_CITYPOWER = "CityPower";
    public static final String ELECTRICITY_PRODUCT_DIKGATLONG = "Dikgatlong";
    public static final String ELECTRICITY_PRODUCT_EKURHULENI = "Ekurhuleni";
    public static final String ELECTRICITY_PRODUCT_EMALAHLENI = "Emalahleni";
    public static final String ELECTRICITY_PRODUCT_ETHEKWINI = "Ethekwini";
    public static final String ELECTRICITY_PRODUCT_GOVAN_MBEKI = "Govan Mbeki";
    public static final String ELECTRICITY_PRODUCT_HBROS = "HBros";
    public static final String ELECTRICITY_PRODUCT_KOKSTAD = "Kokstad";
    public static final String ELECTRICITY_PRODUCT_LANDISANDGYR = "LandisAndGyr";
    public static final String ELECTRICITY_PRODUCT_LANDIS_PREPAID = "Landis Prepaid";
    public static final String ELECTRICITY_PRODUCT_LL_ENERGY = "LL Energy";
    public static final String ELECTRICITY_PRODUCT_MBOMBELA = "Mbombela";
    public static final String ELECTRICITY_PRODUCT_METER_SHACK = "Meter Shack";
    public static final String ELECTRICITY_PRODUCT_TSHWANE = "Tshwane";

    private static String[] electricityProducts = {ELECTRICITY_PRODUCT_AFHCO, ELECTRICITY_PRODUCT_BELA_BELA, ELECTRICITY_PRODUCT_BFW_METERING, ELECTRICITY_PRODUCT_BLUEBERRY, ELECTRICITY_PRODUCT_BROHAM, ELECTRICITY_PRODUCT_CITYPOWER, ELECTRICITY_PRODUCT_DIKGATLONG, ELECTRICITY_PRODUCT_EKURHULENI, ELECTRICITY_PRODUCT_EMALAHLENI, ELECTRICITY_PRODUCT_ETHEKWINI, ELECTRICITY_PRODUCT_GOVAN_MBEKI, ELECTRICITY_PRODUCT_HBROS, ELECTRICITY_PRODUCT_KOKSTAD, ELECTRICITY_PRODUCT_LANDISANDGYR, ELECTRICITY_PRODUCT_LANDIS_PREPAID, ELECTRICITY_PRODUCT_LL_ENERGY, ELECTRICITY_PRODUCT_MBOMBELA, ELECTRICITY_PRODUCT_METER_SHACK, ELECTRICITY_PRODUCT_TSHWANE};

    @Override
    public String getProductCategory() {
        return PRODUCT_CATEGORY;
    }

    @Override
    public String getProduct(int randomNumber) {
        return electricityProducts[randomNumber % electricityProducts.length];
    }
}
