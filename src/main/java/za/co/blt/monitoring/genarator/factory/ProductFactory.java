package za.co.blt.monitoring.genarator.factory;

public interface ProductFactory {

    public String getProductCategory();
    public String getProduct(int randomNumber);
}
