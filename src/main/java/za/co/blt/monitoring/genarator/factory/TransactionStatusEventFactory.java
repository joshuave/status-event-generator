package za.co.blt.monitoring.genarator.factory;

import za.co.blt.monitoring.genarator.model.TransactionStatusEvent;
import za.co.blt.monitoring.random.PseudoRandom;

import java.io.IOException;
import java.net.URISyntaxException;


public class TransactionStatusEventFactory {

    private PseudoRandom pr;


    public TransactionStatusEventFactory() throws IOException, URISyntaxException {
        this.pr = new PseudoRandom();
    }

    public TransactionStatusEvent generate() {
        TransactionStatusEvent transactionStatusEvent = new TransactionStatusEvent();

        ProductFactory productFactory = ProductFactoryFactory.productFactory(this.pr.nextInt());

        transactionStatusEvent.setProductCategory(productFactory.getProductCategory());
        transactionStatusEvent.setProduct(productFactory.getProduct(pr.nextInt()));
        transactionStatusEvent.setOutcome(TransactionOutcomeFactory.getOutcome(pr.nextInt()));

        return transactionStatusEvent;
    }

}
