package za.co.blt.monitoring.genarator.factory;


public class AirtimeProductFactory implements ProductFactory {

    public static final String PRODUCT_CATEGORY = "airtime";

    public static final String AIRTIME_PRODUCT_CELLC = "cellc";
    public static final String AIRTIME_PRODUCT_MTN = "mtn";
    public static final String AIRTIME_PRODUCT_TELKOM = "telkom";
    public static final String AIRTIME_PRODUCT_VIRGIN = "virgin";
    public static final String AIRTIME_PRODUCT_VODACOM = "vodacom";

    private static final String[] airtimeProducts = {AIRTIME_PRODUCT_CELLC, AIRTIME_PRODUCT_MTN, AIRTIME_PRODUCT_TELKOM, AIRTIME_PRODUCT_VIRGIN, AIRTIME_PRODUCT_VODACOM};

    @Override
    public String getProductCategory() {
        return PRODUCT_CATEGORY;
    }

    @Override
    public String getProduct(int randomNumber) {
        return airtimeProducts[randomNumber % airtimeProducts.length];
    }
}
