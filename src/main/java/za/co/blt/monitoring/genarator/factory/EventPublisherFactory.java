package za.co.blt.monitoring.genarator.factory;

import za.co.blt.monitoring.publisher.EventPublisher;
import za.co.blt.monitoring.publisher.RedisPublisher;
import za.co.blt.monitoring.publisher.StdOutPublisher;


public class EventPublisherFactory {

    public static EventPublisher eventPublisher(String[] applicationArguments) {
        if (applicationArguments[0].equals("-r")) {
            return new RedisPublisher(applicationArguments[1], Integer.parseInt(applicationArguments[2]), applicationArguments[3]);
        } else {
            return new StdOutPublisher();
        }
    }
}
