package za.co.blt.monitoring.genarator.model;


public class TransactionStatusEvent {
    private String productCategory;
    private String product;
    private String outcome;

    public String getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(String productCategory) {
        this.productCategory = productCategory;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getOutcome() {
        return outcome;
    }

    public void setOutcome(String outcome) {
        this.outcome = outcome;
    }

    @Override
    public String toString() {
        return "TransactionStatusEvent{" +
                "productCategory='" + productCategory + '\'' +
                ", product='" + product + '\'' +
                ", outcome='" + outcome + '\'' +
                '}';
    }
}
